include(FetchContent)

set(FETCHCONTENT_QUIET FALSE)

######################################################
#####################  vivid #########################
######################################################

FetchContent_Declare(vivid
    GIT_REPOSITORY https://github.com/yellowEmu/vivid.git
    GIT_TAG master
    GIT_PROGRESS TRUE
)

FetchContent_MakeAvailable(vivid)

######################################################
################  bs-thread-pool #####################
######################################################

FetchContent_Declare(bs-thread-pool
    GIT_REPOSITORY https://github.com/bshoshany/thread-pool.git
    GIT_TAG v3.3.0
    GIT_PROGRESS TRUE
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
)

FetchContent_GetProperties(bs-thread-pool)
if(NOT bs-thread-pool_POPULATED)
    FetchContent_Populate(bs-thread-pool)
endif()

add_library(bs-thread-pool INTERFACE)
target_include_directories(bs-thread-pool INTERFACE ${bs-thread-pool_SOURCE_DIR})

######################################################
################  emutools ############################
######################################################

FetchContent_Declare(emutools
    GIT_REPOSITORY https://gitlab.com/yellow_emu/emutools.git
    GIT_TAG main
    GIT_PROGRESS TRUE
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
)

FetchContent_GetProperties(emutools)
if(NOT emutools_POPULATED)
    FetchContent_Populate(emutools)
endif()

add_library(emutools INTERFACE)
target_include_directories(emutools INTERFACE ${emutools_SOURCE_DIR})

######################################################
######################################################
######################################################
