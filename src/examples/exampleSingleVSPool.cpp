#include "FractalRenderer.h"

#include <emutools/timer.h>

#include <cstdlib>
#include <iostream>

using fractalcpp::FractalRenderer;
using emutools::timer;
using std::cout;
using std::endl;

const std::string home(std::getenv("HOME"));

int main()
{
    cout << "hi" << endl;

    int width{ 1920 };
    int height{ 1080 };

    QImage image(width, height, QImage::Format_RGB888);

    FractalRenderer renderer(width, height);
    renderer.setDepth(20000);
    renderer.setColorIntermapType(fractalcpp::ColorIntermapType::FIXED);
    renderer.setRenderMethod(fractalcpp::RenderMethod::SINGLE_THREAD);

    timer timer{};
    renderer.renderAndStore();
    double elapsed{ timer.elapsed() };
    cout << "Single threaded took: " << elapsed << std::endl;

    renderer.paintImage(image);
    std::string filepath0{ home + "/Desktop/image_single.png" };
    image.save(QString::fromStdString(filepath0));

    renderer.setRenderMethod(fractalcpp::RenderMethod::THREAD_POOL);
    timer.reset();
    renderer.renderAndStore();
    elapsed = timer.elapsed();
    cout << "Threadpool took: " << elapsed << std::endl;


    renderer.paintImage(image);
    std::string filepath{ home + "/Desktop/image_pool.png" };
    image.save(QString::fromStdString(filepath));
    std::string filepath2(home + "/Desktop/preview.png");
    renderer.getColorMapPreview(false).save(QString::fromStdString(filepath2));
}