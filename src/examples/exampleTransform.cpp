#include "FractalRenderer.h"

#include <cstdlib>
#include <iostream>

using fractalcpp::FractalRenderer;
using std::cout;
using std::endl;

const std::string home(std::getenv("HOME"));

int main()
{
    cout << "hi" << endl;

    int width{ 1920 };
    int height{ 1080 };

    QImage image(width, height, QImage::Format_RGB888);

    FractalRenderer renderer(width, height);
    renderer.setDepth(200);

    for(int i = 0; i < 10; i++)
    {
        renderer.renderAndStore();
        renderer.paintImage(image);
        std::string filepath{ home + "/Desktop/render/image" + std::to_string(i) + ".png" };
        image.save(QString::fromStdString(filepath));
        renderer.addOffset(-0.1,0.0);
        cout << "file " << i << " written" << endl;
    }
    for(int i = 10; i < 20; i++)
    {
        renderer.renderAndStore();
        renderer.paintImage(image);
        std::string filepath{ home + "/Desktop/render/image" + std::to_string(i) + ".png" };
        image.save(QString::fromStdString(filepath));
        renderer.scaleZoom(1.1);
        cout << "file " << i << " written" << endl;
    }
    for(int i = 20; i < 30; i++)
    {
        renderer.renderAndStore();
        renderer.paintImage(image);
        std::string filepath{ home + "/Desktop/render/image" + std::to_string(i) + ".png" };
        image.save(QString::fromStdString(filepath));
        renderer.addOffset(0.0, 0.02);
        cout << "file " << i << " written" << endl;
    }
    std::string filepath(home + "/Desktop/render/preview.png");
    renderer.getColorMapPreview().save(QString::fromStdString(filepath));

}