#include "FractalRenderer.h"

#include <emutools/timer.h>

#include <cstdlib>
#include <iostream>

using fractalcpp::FractalRenderer;
using emutools::timer;
using std::cout;
using std::endl;

const std::string home(std::getenv("HOME"));

int main()
{
    cout << "hi" << endl;

    int width{ 1920 };
    int height{ 1080 };

    QImage image(width, height, QImage::Format_RGB888);

    FractalRenderer renderer(width, height);
    renderer.setDepth(4000);

    renderer.renderAndStore();
    

    for(int i = 0; i < 60; i++)
    {
        renderer.paintImage(image);
        std::string filepath{ home + "/Desktop/render/image" + std::to_string(i) + ".png" };
        image.save(QString::fromStdString(filepath));
        std::string filepath2(home + "/Desktop/render/preview" + std::to_string(i) + ".png");
        renderer.getColorMapPreview().save(QString::fromStdString(filepath2));
        renderer.setColormapPhaseOffset(static_cast<double>(i) / 30.0);
        cout << "file " << i << " written" << endl;
    }
}