# Set this so find_package will find QT
set(CMAKE_PREFIX_PATH
    "$ENV{QTDIR}"
)

find_package(Qt6 REQUIRED COMPONENTS Gui) 
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

add_executable(exampleTransform
    exampleTransform.cpp
)

target_link_libraries(exampleTransform PRIVATE 
    Qt6::Gui
    fractalcpp
    warning_props
)

add_executable(exampleColorphase
    exampleColorphase.cpp
)

target_link_libraries(exampleColorphase PRIVATE 
    Qt6::Gui
    fractalcpp
    emutools
    warning_props
)

add_executable(exampleSingleVSPool
    exampleSingleVSPool.cpp
)

target_link_libraries(exampleSingleVSPool PRIVATE 
    Qt6::Gui
    fractalcpp
    emutools
    warning_props
)
