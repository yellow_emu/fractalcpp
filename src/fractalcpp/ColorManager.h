#pragma once

#include <vivid/vivid.h>

#include <QColor>
#include <QImage>

#include <string>

namespace fractalcpp
{
    class ColorManager
    {
    private:
        vivid::ColorMap _colormap;
        static vivid::ColorMap::Preset getColorMapPresetFromName(std::string palette);
    public:
        
        /**
         * Init this color manager with the given palette
        */
        ColorManager(std::string paletteName);

        /**
         * Change the palette of this color manager
        */
        void setColorMap(std::string paletteName);

        /**
         * Load a custom palette from a JSON file
        */
        void setCustomPalette(std::string filepath);

        /**
         * Read a color from the colormap by dynamic range
        */
        QColor getColorFromDynamicRange(double value, double colorInterMapPeriods, double phaseOffset) const;

        /**
         * Read a color from the colormap by fixed range
        */
        QColor getColorFromFixedRange(double index, int colorInterMapPeriodLength, double phaseOffset) const;
    };
}
