#include "FractalRenderer.h"

#include "ColorManager.h"

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <vivid/vivid.h>

#include <QImage>
#include <QColor>
#include <QPainter>
#include <QRectF>
#include <QPoint>

#include <BS_thread_pool_light.hpp>

#include <stdexcept>
#include <iostream>

using Eigen::Transform;
using Eigen::Affine;
using Eigen::Translation2d;
using Eigen::Scaling;
using Eigen::AlignedScaling2d;
using Eigen::Vector2d;

namespace fractalcpp
{       
    void FractalRenderer::updateTransformations()
    {
        const double aspectRatio{ _imageWidth / static_cast<double>(_imageHeight) };
        //generate the transformation matrices to convert between screen and world coordinates

        //the "default view" spans four world units in each direction
        //the aspect ratio may stretch the x-axis span (if the image is not square) to not stretch the resulting image
        //the translation is also multiplied by the aspect ratio to center the image
        //so that the world origin is always in the middle of the image
        _screenToWorld =
            Translation2d(Vector2d(_viewOffsetX, _viewOffsetY)) *
            Scaling(1.0 / _viewZoom) * 
            Translation2d(-2.0 * aspectRatio, 2.0) *
            AlignedScaling2d(((4.0 / _imageWidth) * aspectRatio), (-4.0 / _imageHeight))
        ;

        _worldToScreen = _screenToWorld.inverse();
    }

    int FractalRenderer::mandelBrotOptimizedEscape(double x0, double y0, int maxiteration)
    {
        double x2{ 0.0 };
        double y2{ 0.0 };
        double x{ 0.0 };
        double y{ 0.0 };

        int iteration{ 0 };

        while(x2 + y2 <= 4 && iteration < maxiteration)
        {
            y = 2 * x * y + y0;
            x = x2 - y2 + x0;
            x2 = x * x;
            y2 = y * y;
            iteration++;
        }
        
        return iteration;
    }

    FractalRenderer::FractalRenderer(int imageWidth, int imageHeight) :
        _imageWidth{ imageWidth },
        _imageHeight{ imageHeight },
        _escapeIterationValues(imageWidth * imageHeight),
        _colorManager("Inferno")
    {
        updateTransformations();
    }
    
    void FractalRenderer::renderAndStore()
    {
        updateTransformations();
        const int& cols{ _imageWidth };
        const int& rows{ _imageHeight };
        const int& maxiterations{ _renderDepth };

        //Setup the task parameters
        const int numberOfParameters{ cols * rows };
        double* worldXParams { new double[numberOfParameters] };
        double* worldYParams { new double[numberOfParameters] };

        for(int row{ 0 }; row < rows; row++)
        {
            for(int col{ 0 }; col < cols; col++)
            {
                int flatindex{ col + (row * cols) };
                Vector2d screen(col, row);
                Vector2d world{ _screenToWorld * screen };
                worldXParams[flatindex] = world.x();
                worldYParams[flatindex] = world.y();
            }
        }
        //parameters are setup

        //calculate fractal escape values and store them in output array
        if(_renderMethod == RenderMethod::SINGLE_THREAD)
        {
            for(int i{ 0 }; i < numberOfParameters; i++)
            {
                _escapeIterationValues[i] = mandelBrotOptimizedEscape(worldXParams[i], worldYParams[i], maxiterations);
            }
        }
        else
        {
            //Can't capture member variable of object. So, capture pointer to it instead
            auto* pointerToResultContainer = &_escapeIterationValues;
            FractalRenderer::threadPool.push_loop(numberOfParameters,
                [
                    pointerToResultContainer, 
                    worldXParams, 
                    worldYParams, 
                    maxiterations
                ]
                    (const int a, const int b)
                {
                    for(int i{ a }; i < b; i++)
                    {
                        (*pointerToResultContainer)[i] = mandelBrotOptimizedEscape(worldXParams[i], worldYParams[i], maxiterations);
                    }
                }
            );
            FractalRenderer::threadPool.wait_for_tasks();
        }

        delete [] worldXParams;
        delete [] worldYParams;
    }
    
    void FractalRenderer::paintImage(QImage& image) const
    {
        const int& cols{ _imageWidth };
        const int& rows{ _imageHeight };

        for(int col{ 0 }; col < cols; col++)
        {
            for(int row{ 0 }; row < rows; row++)
            {
                int flatindex{ col + (row * cols) };
                const int& iterationValue{ _escapeIterationValues[flatindex] };
                
                QColor color{};
                if(_unescapedPixelsAreBlack && (iterationValue == _renderDepth))
                {
                    color = QColor("black");
                }
                else if(_colorIntermapType == ColorIntermapType::DYNAMIC)
                {
                    double iterationValueNormalized{iterationValue / static_cast<double>(_renderDepth)};
                    color = _colorManager.getColorFromDynamicRange(iterationValueNormalized, _colorDynamicPeriods, _colorIntermapPhaseOffset);
                }
                else
                {
                    color = _colorManager.getColorFromFixedRange(iterationValue, _colorFixedPeriodLength, _colorIntermapPhaseOffset);
                }

                image.setPixelColor(col, row, color);
            }
        }
    }

    void FractalRenderer::visualizeOtherInImage(const FractalRenderer& other, QImage& image) const
    {
        if( other._imageHeight != this->_imageHeight ||
            other._imageWidth != this->_imageWidth)
        {
            throw std::invalid_argument("FractalRenderer::visualizeOtherInImage: the other renderer has a different resolution than this one!");
        }
        //Our and the other's upper left corner und lower right corner are both (0|0) and (_width|_height)
        //respectively. However, they refer to different world coordinates. The goal is to get OUR screen coordinates
        //that describe the world coordinates of the corners of the OTHER renderer.

        //other corner in other frame
        Vector2d otherULCornerOtherScreen(0.0, 0.0);
        Vector2d otherLRCornerOtherScreen(static_cast<double>(_imageWidth), static_cast<double>(_imageHeight));
        //other corner in world frame
        Vector2d otherULCornerWorld{ other._screenToWorld * otherULCornerOtherScreen }; 
        Vector2d otherLRCornerWorld{ other._screenToWorld * otherLRCornerOtherScreen }; 
        //other corner in our frame
        Vector2d otherULCornerMyScreen{ this->_worldToScreen * otherULCornerWorld };
        Vector2d otherLRCornerMyScreen{ this->_worldToScreen * otherLRCornerWorld };

        //Cast results to int (pixel coordinates are whole numbers)
        int upperLeftX{ static_cast<int>(otherULCornerMyScreen.x()) };
        int upperLeftY{ static_cast<int>(otherULCornerMyScreen.y()) };

        int lowerRightX{ static_cast<int>(otherLRCornerMyScreen.x()) };
        int lowerRightY{ static_cast<int>(otherLRCornerMyScreen.y()) };

        //Convert to QPoints
        QPoint upperLeft(upperLeftX, upperLeftY);
        QPoint lowerRight(lowerRightX, lowerRightY);

        //Draw the rectangle into image
        QPainter p(&image);
        p.setPen(QPen(Qt::red, 2));
        p.drawRect(QRectF(upperLeft, lowerRight));
        p.drawLine(upperLeftX, upperLeftY, lowerRightX, lowerRightY);
        p.drawLine(upperLeftX, lowerRightY, lowerRightX, upperLeftY);
        p.end();
    }

    void FractalRenderer::setZoom(double zoom)
    {
        _viewZoom = zoom;
        updateTransformations();
    }

    void FractalRenderer::scaleZoom(double factor)
    {
        _viewZoom *= factor;
        updateTransformations();
    }

    void FractalRenderer::setOffset(double x, double y)
    {
        _viewOffsetX = x;
        _viewOffsetY = y;
        updateTransformations();
    }

    void FractalRenderer::addOffset(double x, double y)
    {
        _viewOffsetX += x;
        _viewOffsetY += y;
        updateTransformations();
    }

    void FractalRenderer::setDepth(int depth)
    {
        _renderDepth = depth;
    }

    void FractalRenderer::setColorPalette(std::string paletteName)
    {
        _colorManager.setColorMap(paletteName);
    }


    void FractalRenderer::setCustomPalette(std::string filepath)
    {
        _colorManager.setCustomPalette(filepath);
    }

    void FractalRenderer::setColorIntermapType(ColorIntermapType type)
    {
        _colorIntermapType = type;
    }

    void FractalRenderer::setDynamicColorInterMapPeriods(double periods)
    {
        _colorDynamicPeriods = periods;
    }

    void FractalRenderer::setFixedColorInterMapPeriodLength(int length)
    {
        _colorFixedPeriodLength = length;
    }

    void FractalRenderer::setColormapPhaseOffset(double phaseOffset)
    {
        _colorIntermapPhaseOffset = phaseOffset;
    }

    void FractalRenderer::setUnescapedPixelsAreBlack(bool unescapedPixelsAreBlack)
    {
        _unescapedPixelsAreBlack = unescapedPixelsAreBlack;
    }

    void FractalRenderer::setRenderMethod(RenderMethod method)
    {
        _renderMethod = method;
    }

    void FractalRenderer::setResolution(int width, int height)
    {
        _imageWidth = width;
        _imageHeight = height;
        _escapeIterationValues.resize(width * height);
        updateTransformations();
    }

    double FractalRenderer::getOffsetX() const
    {
        return _viewOffsetX;
    }

    double FractalRenderer::getOffsetY() const
    {
        return _viewOffsetY;
    }

    double FractalRenderer::getZoom() const
    {
        return _viewZoom;
    }


    int FractalRenderer::getWidth() const
    {
        return _imageWidth;
    }

    int FractalRenderer::getHeight() const
    {
        return _imageHeight;
    }

    QImage FractalRenderer::getColorMapPreview(bool showIntermap) const
    {
        const int width{ 535 };
        const int height{ 30 };

        QImage image(width, height, QImage::Format_RGB888);

        const int& cols{ width };
        const int& rows{ height };

        for(int col{ 0 }; col < cols; col++)
        {
            for(int row{ 0 }; row < rows; row++)
            {
                QColor color{};
                if(!showIntermap)
                {
                    //1.5 instead of 1.0 so the user sees a full period
                    color = _colorManager.getColorFromDynamicRange(col / static_cast<double>(cols), 1.5, 0.0);
                }
                else if(_colorIntermapType == ColorIntermapType::FIXED)
                {
                    color = _colorManager.getColorFromFixedRange(col * (_renderDepth / static_cast<double>(cols)), _colorFixedPeriodLength, _colorIntermapPhaseOffset);
                }
                else
                {
                    color = _colorManager.getColorFromDynamicRange(col / static_cast<double>(cols), _colorDynamicPeriods, _colorIntermapPhaseOffset);
                }
                image.setPixelColor(col, row, color);
            }
        }

        return image;
    }
}