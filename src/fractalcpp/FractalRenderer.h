#pragma once

#include "ColorManager.h"

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <QImage>

#include <BS_thread_pool_light.hpp>

#include <string>
#include <vector>

namespace fractalcpp
{
    enum class ColorIntermapType
    {
        DYNAMIC,
        FIXED
    };

    enum class RenderMethod
    {
        SINGLE_THREAD,
        THREAD_POOL
        //GPU
    };

    class FractalRenderer
    {
    private:
        int _imageWidth;
        int _imageHeight;

        double _viewZoom{ 1.0 };
        
        double _viewOffsetX{ 0.0 };
        double _viewOffsetY{ 0.0 };

        int _renderDepth{ 30 };

        Eigen::Transform<double, 2, Eigen::Affine> _screenToWorld;
        Eigen::Transform<double, 2, Eigen::Affine> _worldToScreen;

        std::vector<int> _escapeIterationValues;

        ColorIntermapType _colorIntermapType{ ColorIntermapType::FIXED };
        double _colorDynamicPeriods{ 1.0 };
        int _colorFixedPeriodLength{ 20 };
        double _colorIntermapPhaseOffset{ 0.0 };
        bool _unescapedPixelsAreBlack{ true };
        RenderMethod _renderMethod{ RenderMethod::THREAD_POOL };

        ColorManager _colorManager;

        static inline BS::thread_pool_light threadPool{};

        void updateTransformations();

        static int mandelBrotOptimizedEscape(double x0, double y0, int maxiteration);
    public:
        /**
         * Initializes this renderer at the "default view"
        */
        FractalRenderer(int imageWidth, int imageHeight);

        /**
         * Render the current view. The escape iteration values are stored internally.
        */
        void renderAndStore();

        /**
         * Populates image with pixel values based on previously calculated escape iteration values.
         * The resolution of image must match the width and height this object was initialized with.
        */
        void paintImage(QImage& image) const;

        /**
         * Visualizes the position of another renderer in the view of this renderer.
         * The other renderer's view will be visualized as a rectangle that will be painted over image.
         * The resolution of image must match the width and height this object was initialized with.
        */
        void visualizeOtherInImage(const FractalRenderer& other, QImage& image) const;
        
        /**
         * Set the view zoom level
        */
        void setZoom(double zoom);

        /**
         * Multiply the view zoom level by factor
        */
        void scaleZoom(double factor);

        /**
         * Set view offset
        */
        void setOffset(double x, double y);

        /**
         * Add p(x|y) to current view offset
        */
        void addOffset(double x, double y);

        /**
         * Set the render depth
        */
        void setDepth(int depth);

        /**
         * Set the color palette for the painter
        */
        void setColorPalette(std::string paletteName);

        /**
         * Set the color palette for the painter
        */
        void setCustomPalette(std::string filepath);

        /**
         * Set whether fixed or dynamic color intermap should be used
        */
        void setColorIntermapType(ColorIntermapType type);

        /**
         * Set the number of periods the dynamic color intermap should span
        */
        void setDynamicColorInterMapPeriods(double periods);

        /**
         * Set the length of a period for the fixed color intermap
        */
        void setFixedColorInterMapPeriodLength(int length);

        /**
         * Set the phase offset for the color intermap
        */
        void setColormapPhaseOffset(double phaseOffset);

        /**
         * Whether unescaped pixels should be black, overriding the colormap
        */
        void setUnescapedPixelsAreBlack(bool unescapedPixelsAreBlack);

        /**
         * Set the render method of this renderer.
        */
        void setRenderMethod(RenderMethod method);

        /**
         * Set the resolution of this renderer
        */
        void setResolution(int width, int height);

        /**
         * Get the current x-coordinate of the view position
        */
        double getOffsetX() const;

        /**
         * Get the current y-coordinate of the view position
        */
        double getOffsetY() const;

        /**
         * Get the current zoom level
        */
        double getZoom() const;

        /**
         * Get the width of this renderer
        */
        int getWidth() const;

        /**
         * Get the height of this renderer
        */
        int getHeight() const;

        /**
         * Get a preview of the effective colormap based on the current intermap settings
         * 
         * The returned image has a dimension of 535x30
        */
        QImage getColorMapPreview(bool showIntermap = true) const;
    };
}
