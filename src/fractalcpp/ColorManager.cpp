#include "ColorManager.h"

#include <vivid/vivid.h>

#include <QColor>
#include <QImage>

#include <string>
#include <string_view>
#include <cmath>
#include <numbers>
#include <unordered_map>

using std::numbers::inv_pi;

static constexpr double two_pi{ 2.0 * std::numbers::pi }; //2 * pi
static constexpr double half_pi{ 0.5 * std::numbers::pi }; // 0.5 * pi

static const std::unordered_map<std::string_view, vivid::ColorMap::Preset> colormapPresets
{
    {"BlueYellow",  vivid::ColorMap::Preset::BlueYellow},
    {"CoolWarm",    vivid::ColorMap::Preset::CoolWarm},
    {"Hsl",         vivid::ColorMap::Preset::Hsl},
    {"HslPastel",   vivid::ColorMap::Preset::HslPastel},
    {"Inferno",     vivid::ColorMap::Preset::Inferno},
    {"Magma",       vivid::ColorMap::Preset::Magma},
    {"Plasma",      vivid::ColorMap::Preset::Plasma},
    {"Rainbow",     vivid::ColorMap::Preset::Rainbow},
    {"Turbo",       vivid::ColorMap::Preset::Turbo},
    {"Viridis",     vivid::ColorMap::Preset::Viridis},
    {"Vivid",       vivid::ColorMap::Preset::Vivid},
};

namespace fractalcpp
{
    vivid::ColorMap::Preset ColorManager::getColorMapPresetFromName(std::string palette)
    {
        auto result{ colormapPresets.at(palette) };
        return result;
    }

    ColorManager::ColorManager(std::string paletteName) :
        _colormap(getColorMapPresetFromName(paletteName)) 
    {

    }

    void ColorManager::setColorMap(std::string paletteName)
    {
        _colormap = vivid::ColorMap(getColorMapPresetFromName(paletteName));
    }

    void ColorManager::setCustomPalette(std::string filepath)
    {
        _colormap = vivid::ColorMap(filepath);
    }

    QColor ColorManager::getColorFromDynamicRange(double value, double colorInterMapPeriods, double phaseOffset) const
    {
        const double& x{ value };
        const double& w{ colorInterMapPeriods };
        const double& o{ phaseOffset };

        const double intermapvalue
        {
            inv_pi *
            std::asin(std::sin(
                (
                    two_pi
                    * ( x - (o / (w - 0.5)) )
                    * ( w - 0.5 )
                )
                - half_pi
            ))
            + 0.5
        };

        const auto vividcolor{ _colormap.at(intermapvalue) };

        return QColor::fromRgbF(vividcolor.x, vividcolor.y, vividcolor.z);
    }

    QColor ColorManager::getColorFromFixedRange(double index, int colorInterMapPeriodLength, double phaseOffset) const
    {
        const double& x{ index };
        const double w{ static_cast<double>(colorInterMapPeriodLength) };
        const double& o{ phaseOffset };

        const double intermapvalue
        {
            inv_pi *
            std::asin(std::sin(
                (
                    two_pi
                    * (x - (2.0 * w * o))
                    * (0.5 / w)
                )
                - half_pi
            ))
            + 0.5
        };

        const auto vividcolor{ _colormap.at(intermapvalue) };

        return QColor::fromRgbF(vividcolor.x, vividcolor.y, vividcolor.z);
    }
}

