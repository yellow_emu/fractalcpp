#pragma once

#include <stdexcept>
#include <cstddef>
#include <vector>
#include <cmath>

std::vector<double> linspace(double start, double end, size_t numElements)
{
    if(start >= end)
    {
        throw std::invalid_argument("linspace: start needs to be smaller than end");
    }
    std::vector<double> result{};
    if(numElements == 0)
    {
        return result;
    }
    if(numElements == 1)
    {
        result.push_back(start);
        return result;
    }
    if(numElements == 2)
    {
        result.push_back(start);
        result.push_back(end);
        return result;
    }
    result.reserve(numElements);

    double diff{ end - start };
    double& offset{ start };

    for(size_t i{ 0 }; i < numElements; i++)
    {
        result.push_back( diff * (i / static_cast<double>(numElements - 1)) + offset );
    }
    return result;
}

std::vector<double> logspaceExpoBase2(double start, double end, size_t numElements)
{
    if(start >= end)
    {
        throw std::invalid_argument("logspaceExpoBase2: start needs to be smaller than end");
    }
    std::vector<double> result{};
    if(numElements == 0)
    {
        return result;
    }
    if(numElements == 1)
    {
        result.push_back(start);
        return result;
    }
    if(numElements == 2)
    {
        result.push_back(start);
        result.push_back(end);
        return result;
    }
    result.reserve(numElements);

    //Calculate the linspace
    std::vector<double> linexpos{ linspace(start , end, numElements) };

    for(size_t i{ 0 }; i < numElements; i++)
    {
        result.push_back(std::pow(2.0, linexpos.at(i)));
    }

    return result;
}

// int main()
// {
//     std::vector<double> nums{ logspaceExpoBase2(std::log2(0.25), std::log2(2.0), 4) };
//     for(const auto& num : nums)
//     {
//         cout << num << endl;
//     }
// }