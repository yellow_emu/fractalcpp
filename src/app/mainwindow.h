#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "FractalRenderer.h"

#include <QMainWindow>
#include <QImage>

#include <string>

using fractalcpp::FractalRenderer;

enum class JobType
{
    PREVIEW,
    DISK,
    DISK_REPAINT
};

enum class Direction
{
    UP,
    DOWN,
    LEFT,
    RIGHT
};

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Ui::MainWindow *ui;

    bool _withRect{ true };

    void connectSignalsToSlots();

    void updateColorPreview();
    void showImageCurrent();
    void showImagePreview();
    void setUIEnabled(bool enabled);
    void setStatus(std::string text);
    void setRunning(std::string text);
    void setLastRender(std::string text);
    void changePreviewZoom(double value, bool zoomIn);
    void changePreviewPosition(Direction direction);
    void setPreviewPosition(double x, double y, double zoom);
    void setProgressBar(int progress);
    void showImageInPreview(QImage image);

    void startRenderJob(JobType type);

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    //these need to be public so the external non-UI job thread can access them
    FractalRenderer* _rendererCurrent;
    FractalRenderer* _rendererPreview;
    FractalRenderer* _rendererDisk;

    QImage* _imageCurrent;
    QImage* _imageCurrentWithRect;
    QImage* _imagePreview;

    int _diskWidth{ 1920 };
    int _diskHeight{ 1080 };
    int _diskSamples{ 3 };
    bool _diskReel{ false };
    QString _diskDir;

    double _savedX{};
    double _savedY{};
    double _savedZoom{ 1.0 };

    //external functions for non-UI thread
    void setStatusExt(std::string text)
        {emit SIGNAL_setStatus(text);}
    void setRunningExt(std::string text)
        {emit SIGNAL_setRunning(text);}
    void setLastRenderExt(std::string text)
        {emit SIGNAL_setLastRender(text);}
    void setUIEnabledExt(bool enabled)
        {emit SIGNAL_setUIEnabled(enabled);}
    void showImageCurrentExt()
        {emit SIGNAL_showImageCurrent();}
    void showImagePreviewExt()
        {emit SIGNAL_showImagePreview();}
    void setProgressBarExt(int progress)
        {emit SIGNAL_setProgressBar(progress);}
    void showImageInPreviewExt(QImage image)
        {emit SIGNAL_showImageInPreview(image);}

signals:
    //signals for non-UI thread
    void SIGNAL_setStatus(std::string text);
    void SIGNAL_setRunning(std::string text);
    void SIGNAL_setLastRender(std::string text);
    void SIGNAL_setUIEnabled(bool enabled);
    void SIGNAL_showImageCurrent();
    void SIGNAL_showImagePreview();
    void SIGNAL_setProgressBar(int);
    void SIGNAL_showImageInPreview(QImage);
    
public slots:

    //slots for non-UI thread
    void SLOT_setStatus(std::string text)
        {setStatus(text);}
    void SLOT_setRunning(std::string text)
        {setRunning(text);}
    void SLOT_setLastRender(std::string text)
        {setLastRender(text);}
    void SLOT_setUIEnabled(bool enabled)
        {setUIEnabled(enabled);}
    void SLOT_showImageCurrent()
        {showImageCurrent();}
    void SLOT_showImagePreview()
        {showImagePreview();}
    void SLOT_setProgressBar(int progress)
        {setProgressBar(progress);}
    void SLOT_showImageInPreview(QImage image)
        {showImageInPreview(image);}

    //regular slots
    void applySlot();
    void renderDiskSlot();
    void repaintCurrentSlot();
    void repaintPreviewSlot();
    void repaintLastRenderSlot();
    void previewSlot();
    void showRectSlot();
    void hideRectSlot();
    void zoomInSlot();
    void zoomOutSlot();
    void upSlot();
    void downSlot();
    void rightSlot();
    void leftSlot();
    void saveSlot();
    void loadSlot();
    void applyManSlot();
    void colorIndexChanged(int index);
    void phaseOffsetChanged(int offsetValue);
    void showInterMapChanged(bool showIntermap);
    void fixedColorChanged(bool selected);
    void fixedPeriodsChanged(int periods);
    void dynamicPeriodsChanged(double periods); 
    void depthChanged(int depth);
    void unescapedBlackChanged(bool unescapedPixelsAreBlack);
    void diskWidthChanged(int width);
    void diskHeightChanged(int height);
    void reelSamplesChanged(int samples);
    void isReelChanged(bool isReel);
    void singleThreadChanged(bool singleThread);
    void multiThreadChanged(bool multiThread);

};
#endif // MAINWINDOW_H
