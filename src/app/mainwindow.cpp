#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include "numscale.h"

#include <emutools/timer.h>

#include <QFileDialog>
#include <QMessageBox>

#include <string>
#include <string_view>
#include <cstddef> 
#include <thread>
#include <atomic>
#include <chrono>
#include <cmath>
#include <iomanip>
#include <sstream>
#include <iostream>

using emutools::timer;
using std::this_thread::sleep_for;
using std::chrono::milliseconds;
using std::chrono::seconds;
using std::cout;
using std::endl;

static constexpr bool g_zoomIn{ true };
static constexpr bool g_zoomOut{ false };

static constexpr std::string_view colormapnames[11]
{
    "BlueYellow",
    "CoolWarm",
    "Hsl",
    "HslPastel",
    "Inferno",
    "Magma",
    "Plasma",
    "Rainbow",
    "Turbo",
    "Viridis",
    "Vivid"
};

MainWindow::MainWindow(QWidget *parent) : 
    QMainWindow(parent),
    ui{ new Ui::MainWindow }
{
    ui->setupUi(this);
    connectSignalsToSlots();

    //Make non-resizable
    setFixedSize(this->size());
    setWindowTitle("FractalCPP");

    //Initialize the renderers
    _rendererCurrent = new FractalRenderer(480, 270);
    _rendererPreview = new FractalRenderer(480, 270);
    _rendererDisk = new FractalRenderer(480, 270);

    //Initialize the images
    _imageCurrent           = new QImage(480, 270, QImage::Format_RGB888);
    _imageCurrentWithRect   = new QImage(480, 270, QImage::Format_RGB888);
    _imagePreview           = new QImage(480, 270, QImage::Format_RGB888);

    //Render the default view for both renderers
    _rendererCurrent->renderAndStore();
    _rendererPreview->renderAndStore();
    _rendererDisk->renderAndStore();

    //Fill the images with pixels
    _rendererCurrent->paintImage(*_imageCurrent);
    *_imageCurrentWithRect = _imageCurrent->copy();
    _rendererPreview->paintImage(*_imagePreview);

    //Show the images (apply QImages to labels)
    showImageCurrent();
    showImagePreview();

    //Initialize the color palette combo box
    for(int i{ 0 }; i < 11; i++)
    {
        ui->comboBoxColor->addItem(QString::fromStdString(std::string(colormapnames[i])));
    }
    ui->comboBoxColor->addItem("Custom"); //11

    ui->comboBoxColor->setCurrentIndex(4);
}

void MainWindow::connectSignalsToSlots()
{
    QObject::connect(
        this,
        SIGNAL(SIGNAL_setStatus(std::string)),
        this,
        SLOT(SLOT_setStatus(std::string))
    );

    QObject::connect(
        this,
        SIGNAL(SIGNAL_setRunning(std::string)),
        this,
        SLOT(SLOT_setRunning(std::string))
    );

    QObject::connect(
        this,
        SIGNAL(SIGNAL_setLastRender(std::string)),
        this,
        SLOT(SLOT_setLastRender(std::string))
    );

    QObject::connect(
        this,
        SIGNAL(SIGNAL_setUIEnabled(bool)),
        this,
        SLOT(SLOT_setUIEnabled(bool))
    );

    QObject::connect(
        this,
        SIGNAL(SIGNAL_showImageCurrent()),
        this,
        SLOT(SLOT_showImageCurrent())
    );

    QObject::connect(
        this,
        SIGNAL(SIGNAL_showImagePreview()),
        this,
        SLOT(SLOT_showImagePreview())
    );

    QObject::connect(
        this,
        SIGNAL(SIGNAL_setProgressBar(int)),
        this,
        SLOT(SLOT_setProgressBar(int))
    );

    QObject::connect(
        this,
        SIGNAL(SIGNAL_showImageInPreview(QImage)),
        this,
        SLOT(SLOT_showImageInPreview(QImage))
    );
}

//########################################################
//################  HELPER FUNCTIONS      ################
//########################################################

void MainWindow::updateColorPreview()
{
    bool showIntermap{ ui->checkBoxShowIntermap->isChecked() };
    ui->labelImageColorPreview->setPixmap(QPixmap::fromImage(_rendererCurrent->getColorMapPreview(showIntermap)));
}

//Apply QImage to label
void MainWindow::showImageCurrent()
{
    if(_withRect)
    {   
        //update the rect

        //reset the image (clear previous rects)
        *_imageCurrentWithRect = _imageCurrent->copy();
        //draw new rect
        _rendererCurrent->visualizeOtherInImage(*_rendererPreview, *_imageCurrentWithRect);
        //show it
        ui->labelImageCurrent->setPixmap(QPixmap::fromImage(*_imageCurrentWithRect));
    }
    else
    {
        ui->labelImageCurrent->setPixmap(QPixmap::fromImage(*_imageCurrent));
    }

    //Show current position on UI
    
    ui->labelCurPosX->setText(QString::number(_rendererCurrent->getOffsetX(), 'g', 15));
    ui->labelCurPosY->setText(QString::number(_rendererCurrent->getOffsetY(), 'g', 15));
    ui->labelCurZoom->setText(QString::number(_rendererCurrent->getZoom(), 'g', 15));
}

//Apply QImage to label
void MainWindow::showImagePreview()
{
    ui->labelImagePreview->setPixmap(QPixmap::fromImage(*_imagePreview));
}

void MainWindow::setUIEnabled(bool enabled)
{
    ui->pushButtonRenderApply->setEnabled(enabled);
    ui->pushButtonRenderPreview->setEnabled(enabled);
    ui->pushButtonRepaint->setEnabled(enabled);
    ui->pushButtonRepaint_2->setEnabled(enabled);
    ui->pushButtonApplyRenderDisk->setEnabled(enabled);
    ui->spinBoxRenderWidth->setEnabled(enabled);
    ui->spinBoxRenderHeight->setEnabled(enabled);
    ui->checkBoxMakeReel->setEnabled(enabled);
    ui->spinBoxReelSamples->setEnabled(enabled);
    ui->radioButtonSingleThreaded->setEnabled(enabled);
    ui->radioButtonMultiThreaded->setEnabled(enabled);
    ui->comboBoxColor->setEnabled(enabled);
    ui->horizontalSlider->setEnabled(enabled);
    ui->radioButtonColorFixed->setEnabled(enabled);
    ui->radioButtonColorDynamic->setEnabled(enabled);
    ui->spinBoxColorFixPeriods->setEnabled(enabled);
    ui->doubleSpinBoxColorDynamicPeriods->setEnabled(enabled);
    ui->checkBoxShowIntermap->setEnabled(enabled);
    ui->spinBoxDepth->setEnabled(enabled);
    ui->checkBoxUnescapeIsBlack->setEnabled(enabled);
    ui->pushButtonRectShow->setEnabled(enabled);
    ui->pushButtonRectHide->setEnabled(enabled);
    ui->pushButtonZoomIn->setEnabled(enabled);
    ui->pushButtonZoomOut->setEnabled(enabled);
    ui->doubleSpinBoxZoomRate->setEnabled(enabled);
    ui->pushButtonLeft->setEnabled(enabled);
    ui->pushButtonRight->setEnabled(enabled);
    ui->pushButtonUp->setEnabled(enabled);
    ui->pushButtonDown->setEnabled(enabled);
    ui->doubleSpinBoxManX->setEnabled(enabled);
    ui->doubleSpinBoxManY->setEnabled(enabled);
    ui->doubleSpinBoxManZoom->setEnabled(enabled);
    ui->pushButtonManApply->setEnabled(enabled);
    ui->pushButtonSavePos->setEnabled(enabled);
    ui->pushButtonLoadPos->setEnabled(enabled);
    ui->pushButtonRepaintLastRender->setEnabled(enabled);
}

void MainWindow::setStatus(std::string text)
{
    ui->labelStatus->setText(QString::fromStdString(text));
}

void MainWindow::setRunning(std::string text)
{
    ui->labelRunningFor->setText(QString::fromStdString(text));
}

void MainWindow::setLastRender(std::string text)
{
    ui->labelLastTime->setText(QString::fromStdString(text));
}

void MainWindow::changePreviewZoom(double value, bool zoomIn)
{
    _rendererPreview->scaleZoom(zoomIn ? value : 1.0 / value);

    showImageCurrent();
}

void MainWindow::changePreviewPosition(Direction direction)
{
    
    double currentZoomLevel{ _rendererPreview->getZoom() };
    double offsetStep{ 0.27 / currentZoomLevel };

    switch(direction)
    {
        case Direction::UP:
        {
            _rendererPreview->addOffset(0.0, offsetStep);
            break;
        }
        case Direction::DOWN:
        {
            _rendererPreview->addOffset(0.0, -offsetStep);
            break;
        }
        case Direction::LEFT:
        {
            _rendererPreview->addOffset(-offsetStep, 0.0);
            break;
        }
        case Direction::RIGHT:
        {
            _rendererPreview->addOffset(offsetStep, 0.0);
            break;
        }
    }
    
    //This will re-render the preview rect
    showImageCurrent();
}

void MainWindow::setPreviewPosition(double x, double y, double zoom)
{
    _rendererPreview->setOffset(x, y);
    _rendererPreview->setZoom(zoom);

    //This will re-render the preview rect
    showImageCurrent();
}

void MainWindow::setProgressBar(int progress)
{
    ui->progressBar->setValue(progress);
}

void MainWindow::showImageInPreview(QImage image)
{
    ui->labelImagePreview->setPixmap(QPixmap::fromImage(image));
}

//########################################################
//################   RENDER JOB FUNCTIONS      ###########
//########################################################

void timerPoller(MainWindow* window, std::atomic_bool* keepPolling)
{
    // cout << "timer poller started" << endl;
    timer timer{};
    while(*keepPolling)
    {
        std::stringstream stream{};
        stream << std::fixed << std::setprecision(1) << timer.elapsed();
        std::string elapsedString{ stream.str() };
        window->setRunningExt(elapsedString);
        sleep_for(milliseconds(100));
    }
    // cout << "timer poller ended" << endl;
    delete keepPolling;
}

void renderJobTask(MainWindow* window, JobType type)
{
    // cout << "renderer started" << endl;
    window->setStatusExt("Rendering");

    //Start the time poller
    std::atomic_bool* keepPolling{ new std::atomic_bool{true} };
    std::thread t(timerPoller, window, keepPolling);
    t.detach();

    static int counter{ 0 };
    switch(type)
    {
        //render and set preview
        case JobType::PREVIEW:
        {
            //Start timer
            timer timer{};
            //Start rendering
            window->_rendererPreview->renderAndStore();
            //Stop time
            double elapsed{ timer.elapsed() };
            //Paint preview image
            window->_rendererPreview->paintImage(*(window->_imagePreview));
            //Show image
            window->showImagePreviewExt();

            //Show time on UI
            std::stringstream stream{};
            stream << std::fixed << std::setprecision(1) << elapsed;
            std::string elapsedString{ stream.str() };
            window->setLastRenderExt(elapsedString);
            //Done
            break;
        }

        //Create reel and write files to disk
        case JobType::DISK:
        {
            if(window->_diskReel)
            {
                //get desired resolution
                int width{ window->_diskWidth };
                int height{ window->_diskHeight };
                //create renderer and configure it
                *(window->_rendererDisk) = *(window->_rendererCurrent);
                window->_rendererDisk->setResolution(width, height);

                int numberOfSamples{ window->_diskSamples };

                double zoomLevelStart{ window->_savedZoom };
                double zoomLevelEnd{ window->_rendererDisk->getZoom() };

                // cout << "Number of samples: " << numberOfSamples << endl;
                // cout << "Start level: " << zoomLevelStart << endl;
                // cout << "End level: " << zoomLevelEnd << endl;

                //This vector contains every zoomlevel that needs to be rendered.
                std::vector<double> zoomLevels{ logspaceExpoBase2(std::log2(zoomLevelStart), std::log2(zoomLevelEnd), numberOfSamples) };

                timer timer{};
                QImage diskImage(width, height, QImage::Format_RGB888);
                //Execute all jobs sequentially
                for(size_t i{ 0 }; i < zoomLevels.size(); i++)
                {
                    double zoomLevel{ zoomLevels.at(i) };
                    window->_rendererDisk->setZoom(zoomLevel);
                    //render
                    timer.reset();
                    window->_rendererDisk->renderAndStore();
                    double elapsed{ timer.elapsed() };
                    //paint the image
                    window->_rendererDisk->paintImage(diskImage);
                    //Write image to disk
                    std::string path{ window->_diskDir.toStdString() + "/fractal" + std::to_string(++counter) + ".png" };
                    diskImage.save(QString::fromStdString(path));

                    //Show time on UI
                    std::stringstream stream{};
                    stream << std::fixed << std::setprecision(1) << elapsed;
                    std::string elapsedString{ stream.str() };
                    window->setLastRenderExt(elapsedString);
                    window->setProgressBarExt( (static_cast<double>(i + 1) / static_cast<double>(zoomLevels.size())) * 100.0 );
                    window->showImageInPreviewExt(diskImage);
                }
                //done
            }
            else
            {
                //get desired resolution
                int width{ window->_diskWidth };
                int height{ window->_diskHeight };
                //create renderer and configure it
                *(window->_rendererDisk) = *(window->_rendererCurrent);
                window->_rendererDisk->setResolution(width, height);
                //render
                timer timer{};
                window->_rendererDisk->renderAndStore();
                double elapsed{ timer.elapsed() };
                //create output image
                QImage diskImage(width, height, QImage::Format_RGB888);
                //paint the image
                window->_rendererDisk->paintImage(diskImage);
                //Write image to disk
                std::string path{ window->_diskDir.toStdString() + "/fractal" + std::to_string(++counter) + ".png" };
                diskImage.save(QString::fromStdString(path));

                //Show time on UI
                std::stringstream stream{};
                stream << std::fixed << std::setprecision(1) << elapsed;
                std::string elapsedString{ stream.str() };
                window->setLastRenderExt(elapsedString);
                //done
            }
            break;
        }

        case JobType::DISK_REPAINT:
        {
            int width{ window->_rendererDisk->getWidth() };
            int height{ window->_rendererDisk->getHeight() };
            //The disk renderer has the changed color properties from preview
            //and still has the render values from the last render. 
            //Just repaint the image and save it
            //create output image
            QImage diskImage(width, height, QImage::Format_RGB888);
            //paint the image
            window->_rendererDisk->paintImage(diskImage);
            //Write image to disk
            std::string path{ window->_diskDir.toStdString() + "/fractal" + std::to_string(++counter) + ".png" };
            diskImage.save(QString::fromStdString(path));
            //done

            break;
        }
    }

    //cleanup
    *keepPolling = false; //this kills the timer thread
    window->setUIEnabledExt(true);
    window->setStatusExt("Idle");
    // cout << "renderer ended" << endl;
}

void MainWindow::startRenderJob(JobType type)
{
    setUIEnabled(false);
    std::thread t(renderJobTask, this, type);
    t.detach();
}

//########################################################
//################   UI SLOTS      #######################
//########################################################

void MainWindow::applySlot()
{
    //Apply current selection
    *_rendererCurrent = *_rendererPreview;
    //Paint over all images
    _rendererPreview->paintImage(*_imageCurrent);
    _rendererPreview->paintImage(*_imageCurrentWithRect);
    _rendererPreview->paintImage(*_imagePreview);

    showImageCurrent();
    showImagePreview();
}

void MainWindow::renderDiskSlot()
{
    QString dir;
    dir = QFileDialog::getExistingDirectory(
        this, //Inside Mainwindow class
        tr("Open Directory"),
        "",
        QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks
    );

    if (dir.isEmpty())
    {
        QMessageBox msgBox;
        msgBox.setText("No custom directory was selected.");
        msgBox.exec();
        return;
    }


    _diskDir = dir;
    startRenderJob(JobType::DISK);
}

void MainWindow::previewSlot()
{
    startRenderJob(JobType::PREVIEW);
}

void MainWindow::showRectSlot()
{
    _withRect = true;
    showImageCurrent();
}

void MainWindow::hideRectSlot()
{
    _withRect = false;
    showImageCurrent();
}

void MainWindow::zoomInSlot()
{
    double zoomRate{ ui->doubleSpinBoxZoomRate->value() };
    changePreviewZoom(zoomRate, g_zoomIn);
}

void MainWindow::zoomOutSlot()
{
    double zoomRate{ ui->doubleSpinBoxZoomRate->value() };
    changePreviewZoom(zoomRate, g_zoomOut);
}

void MainWindow::upSlot()
{
    changePreviewPosition(Direction::UP);
}

void MainWindow::downSlot()
{
    changePreviewPosition(Direction::DOWN);
}

void MainWindow::rightSlot()
{
    changePreviewPosition(Direction::RIGHT);
}

void MainWindow::leftSlot()
{
    changePreviewPosition(Direction::LEFT);
}

void MainWindow::saveSlot()
{
    _savedX = _rendererCurrent->getOffsetX();
    _savedY = _rendererCurrent->getOffsetY();
    _savedZoom = _rendererCurrent->getZoom();

    ui->labelSavePosX->setText(QString::number(_savedX, 'g', 15));
    ui->labelSavePosY->setText(QString::number(_savedY, 'g', 15));
    ui->labelSaveZoom->setText(QString::number(_savedZoom, 'g', 15));
}

void MainWindow::loadSlot()
{
    setPreviewPosition(_savedX, _savedY, _savedZoom);
}

void MainWindow::applyManSlot()
{
    double x{ ui->doubleSpinBoxManX->value() };
    double y{ ui->doubleSpinBoxManY->value() };
    double zoom{ ui->doubleSpinBoxManZoom->value() };
    setPreviewPosition(x, y, zoom);
}

void MainWindow::repaintCurrentSlot()
{
    _rendererCurrent->paintImage(*_imageCurrent);
    _rendererCurrent->paintImage(*_imageCurrentWithRect);
    showImageCurrent();
}

void MainWindow::repaintPreviewSlot()
{
    _rendererPreview->paintImage(*_imagePreview);
    showImagePreview();
}

void MainWindow::repaintLastRenderSlot()
{
    startRenderJob(JobType::DISK_REPAINT);
}

void MainWindow::colorIndexChanged(int index)
{
    if(index == 11)
    {
        QString dir;
        dir = QFileDialog::getOpenFileName(
            this, //Inside Mainwindow class
            tr("Open File"),
            "",
            tr("Color Maps (*.json)")
        );

        if (dir.isEmpty())
        {
            QMessageBox msgBox;
            msgBox.setText("No custom directory was selected.");
            msgBox.exec();
            return;
        }

        _rendererCurrent->setCustomPalette(dir.toStdString());
        _rendererPreview->setCustomPalette(dir.toStdString());
        _rendererDisk->setCustomPalette(dir.toStdString());
    }
    else
    {
        _rendererCurrent->setColorPalette(std::string(colormapnames[index]));
        _rendererPreview->setColorPalette(std::string(colormapnames[index]));
        _rendererDisk->setColorPalette(std::string(colormapnames[index]));
    }

    updateColorPreview();
}

void MainWindow::phaseOffsetChanged(int offsetValue)
{
    _rendererCurrent->setColormapPhaseOffset(offsetValue / 100.0);
    _rendererPreview->setColormapPhaseOffset(offsetValue / 100.0);
    _rendererDisk->setColormapPhaseOffset(offsetValue / 100.0);

    updateColorPreview();
}

void MainWindow::showInterMapChanged(bool showIntermap)
{
    ui->labelImageColorPreview->setPixmap(QPixmap::fromImage(_rendererCurrent->getColorMapPreview(showIntermap)));
}

void MainWindow::fixedColorChanged(bool selected)
{
    if(selected)
    {
        _rendererCurrent->setColorIntermapType(fractalcpp::ColorIntermapType::FIXED);
        _rendererPreview->setColorIntermapType(fractalcpp::ColorIntermapType::FIXED);
        _rendererDisk->setColorIntermapType(fractalcpp::ColorIntermapType::FIXED);
    }
    else
    {
        _rendererCurrent->setColorIntermapType(fractalcpp::ColorIntermapType::DYNAMIC);
        _rendererPreview->setColorIntermapType(fractalcpp::ColorIntermapType::DYNAMIC);
        _rendererDisk->setColorIntermapType(fractalcpp::ColorIntermapType::DYNAMIC);
    }

    updateColorPreview();
}

void MainWindow::fixedPeriodsChanged(int periods)
{
    _rendererCurrent->setFixedColorInterMapPeriodLength(periods);
    _rendererPreview->setFixedColorInterMapPeriodLength(periods);
    _rendererDisk->setFixedColorInterMapPeriodLength(periods);

    updateColorPreview();
}

void MainWindow::dynamicPeriodsChanged(double periods)
{
    _rendererCurrent->setDynamicColorInterMapPeriods(periods);
    _rendererPreview->setDynamicColorInterMapPeriods(periods);
    _rendererDisk->setDynamicColorInterMapPeriods(periods);

    updateColorPreview();
}

void MainWindow::depthChanged(int depth)
{
    _rendererPreview->setDepth(depth);

    updateColorPreview();
}

void MainWindow::unescapedBlackChanged(bool unescapedPixelsAreBlack)
{
    _rendererCurrent->setUnescapedPixelsAreBlack(unescapedPixelsAreBlack);
    _rendererPreview->setUnescapedPixelsAreBlack(unescapedPixelsAreBlack);
    _rendererDisk->setUnescapedPixelsAreBlack(unescapedPixelsAreBlack);
}

void MainWindow::diskWidthChanged(int width)
{
    _diskWidth = width;
}

void MainWindow::diskHeightChanged(int height)
{
    _diskHeight = height;
}

void MainWindow::reelSamplesChanged(int samples)
{
    _diskSamples = samples;
}

void MainWindow::isReelChanged(bool isReel)
{
    _diskReel = isReel;
}

void MainWindow::singleThreadChanged(bool singleThread)
{
    if(singleThread)
    {
        _rendererCurrent->setRenderMethod(fractalcpp::RenderMethod::SINGLE_THREAD);
        _rendererPreview->setRenderMethod(fractalcpp::RenderMethod::SINGLE_THREAD);
    }
}

void MainWindow::multiThreadChanged(bool multiThread)
{
    if(multiThread)
    {
        _rendererCurrent->setRenderMethod(fractalcpp::RenderMethod::THREAD_POOL);
        _rendererPreview->setRenderMethod(fractalcpp::RenderMethod::THREAD_POOL);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

