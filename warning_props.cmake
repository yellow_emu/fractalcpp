add_library(warning_props INTERFACE)

# Enable ALL warnings
target_compile_options(warning_props INTERFACE
    #-Wall
    #-Wextra
    #-Wpedantic
)
