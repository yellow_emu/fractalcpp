
# fractalcpp
Fractals in C++!
Explore the Mandelbrot set and create pretty images.

## Interface
![Imgur](https://i.imgur.com/GKQXl53.png)

## Custom color palettes
You can use custom color palettes by defining them in a JSON file.
An example for a custom color palette can be found under `src/fractalcpp/custompalettes`. To use your custom palette, select "Custom" under the palette selection and select your custom palette JSON file.

## GPU Rendering support
Support for parallel GPU computing exsists on the `cuda` branch. (Recommended for high depth images as it will be much faster. However, only supported with CUDA-capable NVidia graphics cards)

## Dependencies

Most dependencies are fetched automatically via conan or CMake's fetch content.

However, you'll have to install these libraries yourself:

 - Qt 6.6.1 - Also set an environment variable "QTDIR" that points to your main installation path (or directly insert the path - check CMake files)

## Build with Conan
From the root of this project, run

`conan install . --output-folder=build --build=missing`

to initialize conan and make it fetch all needed libraries. Then run

`cmake -B build --preset conan-default` <br>
`cmake --build --preset conan-release`

to configure and build the project.

To also have a debug configuration, use

`conan install . --output-folder=build --build=missing --settings=build_type=Debug`

and build with

`cmake --build --preset conan-debug`

  
## Sample Images

![Imgur](https://i.imgur.com/xk82ziJ.png)
![Imgur](https://i.imgur.com/KN4LCWx.png)
![Imgur](https://i.imgur.com/4BhHeO8.png)
![Imgur](https://i.imgur.com/tz7jElv.png)
![Imgur](https://i.imgur.com/XAtVxuF.png)
![Imgur](https://i.imgur.com/6IAk5yF.png)
![Imgur](https://i.imgur.com/HLOm4pY.png)
![Imgur](https://i.imgur.com/HYamH6A.png)
![Imgur](https://i.imgur.com/TKizXGM.png)
![Imgur](https://i.imgur.com/wc75B7g.png)
![Imgur](https://i.imgur.com/AGV7ymA.png)
![Imgur](https://i.imgur.com/VnkhHnq.png)
![Imgur](https://i.imgur.com/yyN4ek8.png)
![Imgur](https://i.imgur.com/F79o0kd.png)
![Imgur](https://i.imgur.com/yTxreAR.png)
![Imgur](https://i.imgur.com/rbiH5pZ.jpg)
![Imgur](https://i.imgur.com/E7joNlA.png)